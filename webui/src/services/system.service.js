/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import axios from 'axios'

const systemService = {
  server: null,

  actionPowerOff () {
    axios.get(`${this.getServer()}/power/off`)
      .then((response) => {
        if (response.status !== '200') {
          console.error(response)
        }
      })
  },

  actionReboot () {
    axios.get(`${this.getServer()}/power/reboot`)
      .then((response) => {
        if (response.status !== '200') {
          console.error(response)
        }
      })
  },
  // get the base-URL of the Service
  getServer () {
    if (this.server) {
      return this.server
    }
    if (process.env.VUE_APP_RASPBERRY_SERVICE_SERVER) {
      this.server = process.env.VUE_APP_RASPBERRY_SERVICE_SERVER
    } else {
      this.server = window.location.origin
      if (this.server.charAt(this.server.length - 1) === '/') {
        this.server = this.server.substring(0, this.server.length - 1)
      }
    }
    console.info(`Raspberry-Server: ${this.server}`)
    return this.server
  }
}

export default systemService
