/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import axios from 'axios'

const overlayService = {
  server: null,
  async getImagesList () {
    const response = await axios.get(`${this.getServer()}/overlayPictures/getAll`)

    if (response.status === 200 && response.data.return.msg === 'OK') {
      let sortedList = response.data.return.data.pictures.sort((a, b) => a.filename < b.filename ? -1 : 1)
      sortedList = sortedList.map(item => ({
        filename: item.filename,
        fixed: item.fixed,
        href: `${this.getServer()}/overlayImg/${item.filename}`
      }))
      return sortedList
    }
    console.error('loading overlay images failed', response)
  },
  async addNewOverlayImage (formData) {
    const config = {
      headers: {
      //  'content-type': 'multipart/form-data'
      }
    }
    // const formData = new FormData()
    // formData.append('file', file)
    // formData.append('filename', file.name)
    const response = await axios.post(`${this.getServer()}/overlayPictures/upload`, formData, config)
    return response
  },
  async deleteOverlayImage (file) {
    const formData = new FormData()
    formData.append('files', JSON.stringify([{ filename: file }]))
    const response = await axios.post(`${this.getServer()}/overlayPictures/delete`, formData)
    return response
  },
  async setImageOverlay (filename) {
    const response = await axios.post(`${this.getServer()}/overlayGenerator/overlayImage`, {
      path: filename
    })
    if (response.status !== 200) {
      console.error('setting image overlay failed', response)
    }
  },
  async clearOverlayImage () {
    // TODO axios.delete(`${this.getServer()}/overlayGenerator/overlayImage`)
    await axios.post(`${this.getServer()}/overlayGenerator/clearOverlayImage`)
  },
  getSupportedImageFormat () {
    return [
      'image/png',
      'image/jpg',
      'image/jpeg'
    ]
  },
  async setHymnOverlay (hymn, strophe) {
    const response = await axios.post(`${this.getServer()}/overlayGenerator/hymn`, {
      hymn: hymn, verse: strophe
    })
    if (response.status !== 200) {
      console.error('setting image overlay failed', response)
    } else if (response.data.return.rc === 1) {
      console.error(response.data.return.msg)
    }
  },
  async setTextOverlay (left, right) {
    const response = await axios.post(`${this.getServer()}/overlayGenerator/overlayText`, {
      left, right
    })
    if (response.status === 200) {
      return response.data.return
    }
    console.error('setting image overlay failed', response)
  },
  async clearTextOverlay () {
    const response = await axios.post(`${this.getServer()}/overlayGenerator/clearOverlayText`)
    if (response.status !== 200) {
      console.error('clearing texte overlay failed', response)
    }
  },
  /**
   * Get the base-URL of the Service
   */
  getServer () {
    if (this.server) {
      return this.server
    }
    if (process.env.VUE_APP_OVERLAY_SERVICE_SERVER) {
      this.server = process.env.VUE_APP_OVERLAY_SERVICE_SERVER
    } else {
      this.server = window.location.origin
      if (this.server.charAt(this.server.length - 1) === '/') {
        this.server = this.server.substring(0, this.server.length - 1)
      }
    }
    return this.server
  }
}

export default overlayService
