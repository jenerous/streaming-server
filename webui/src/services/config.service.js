/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import axios from 'axios'

const confService = {
  server: null,
  async startStream () {
    const response = await axios.get(`${this.getServer()}/conf?streaming=start`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error('start failed', response)
  },
  async stopStream () {
    const response = await axios.get(`${this.getServer()}/conf?streaming=stop`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error('stop failed', response)
  },

  async receiveStream () {
    const response = await axios.get(`${this.getServer()}/conf?streaming=receive`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error('receive failed', response)
  },
  async get (property) {
    const response = await axios.get(`${this.getServer()}/conf?getProperty=${property}`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error(response)
  },
  async set (property, value) {
    const response = await axios.get(`${this.getServer()}/conf?setProperty=${property}&value=${value}`)
    if (response.status === 200) {
      return response.data.return
    }
    console.error(response)
  },
  async getStatus (property) {
    const response = await axios.get(`${this.getServer()}/conf?status=`)
    if (response.status === 200) {
      return response.data.status
    }
    console.error(response)
  },
  async getROProperties () {
    const response = await axios.get(`${this.getServer()}/conf?getROProperties=`)
    if (response.status === 200) {
      return response.data
    }
    console.error(response)
  },
  /**
   * Get the base-URL of the Service
   */
  getServer () {
    if (this.server) {
      return this.server
    }
    if (process.env.VUE_APP_CONFIG_SERVICE_SERVER) {
      this.server = process.env.VUE_APP_CONFIG_SERVICE_SERVER
    } else {
      this.server = window.location.origin
      if (this.server.charAt(this.server.length - 1) === '/') {
        this.server = this.server.substring(0, this.server.length - 1)
      }
    }
    return this.server
  }
}

export default confService
