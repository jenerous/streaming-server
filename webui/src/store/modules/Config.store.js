/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import configService from '@/services/config.service'

export const namespaced = true

export const state = {
  serverStatus: null
}

export const actions = {
  setServerStatus ({ commit }) {
    configService.getStatus()
      .then((result) => {
        commit('SET_CONFIG_STATE', result)
      })
  },
  startStream ({ commit, dispatch }) {
    configService.startStream()
      .then((result) => {
        dispatch('setServerStatus')
        // the status is delayed for starting server stream.
        // We have to wait and update again
        setTimeout(() => {
          dispatch('setServerStatus')
        }, 3000)
      })
  },
  stopStream ({ commit, dispatch }) {
    configService.stopStream()
      .then((result) => {
        dispatch('setServerStatus')
      })
  },
  receiveStream ({ commit, dispatch }) {
    configService.receiveStream()
      .then((result) => {
        dispatch('setServerStatus')
        // the status is delayed for starting server stream.
        // We have to wait and update again
        setTimeout(() => {
          dispatch('setServerStatus')
        }, 3000)
      })
  }
}

export const getters = {
  serverStatus: (state) => {
    return state.serverStatus
  }
}

export const mutations = {
  SET_CONFIG_STATE (state, serverStatus) {
    state.serverStatus = serverStatus
  }
}
