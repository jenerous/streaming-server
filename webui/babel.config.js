/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
}
