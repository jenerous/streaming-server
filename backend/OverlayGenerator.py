# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0
# @TODO refactor variable names

from PIL import Image, ImageDraw, ImageFont
import csv
import textwrap
import os
import tempfile
from pathvalidate import sanitize_filename

from twisted.web import resource
from twisted.web import server
import json


class OverlayGenerator(resource.Resource):
    isLeaf = True
    cR = None

    overlayMessageLeft = None
    overlayMessageRight = None
    overlayImage = None

    overlayLastImg = None


    
    def __init__(self, cR):
        self.cR = cR
        self.cR.setOverlayGen_i(self)

    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    def wrapTextDynamically(self, text, text_size):
        """Wrap text to fit into lower third

        Args:
        text (String): Text to wrap

        Returns:
            String: Text with linebreaks
        """
        r = ""
        for t in text.splitlines():
            r += "\n".join(textwrap.wrap(t, width = text_size))
            r += "\n"

        return r

    def generateOverlay(self, destination_path):
        """
        Generates an overlay png image based on different messages

        Args:
            destination_path (String): path and filename where the png will be saved to
            mode (String): Set mode for lower third. See parameter msg
            overlay_path (String): path for the overlay image or None in case of no overlay picture is provided.
            msg (String): Message to display, format depends on mode set.
                Mode       *   msg
                -----------------------------------------
                hymn       *   <number>#<verse> (verse is optional, # not)
                dynamic    *   <top left string> # <bottom left String> # <right string>

            dynamic mode can be used if a error message has to be displayed, in case of strange things happen.

        """
        # Variable definitions
        logo_size = 70, 70
        image_size = 1280, 720
        fontPath = "/usr/share/fonts/truetype/liberation/LiberationMono-Regular.ttf"
        fontPathBold = "/usr/share/fonts/truetype/liberation/LiberationSans-Bold.ttf"
        logoFile = "OverlayGenerator/logo.png"

        # Overlay generation
        overlay = Image.new("RGBA", image_size, (255, 255, 255, 0))
        overlay_image = ImageDraw.Draw(overlay)

        # Insert Overlay Picture, if present
        if(self.overlayImage != None):
            background_orig = Image.open(self.overlayImage).convert("RGBA")
            background = background_orig.resize(image_size)
            overlay.paste(background, (0,0), background)

        """
        Depending on parameter "mode", template for lower third is chosen.
        """
        if(self.overlayMessageLeft != None and self.overlayMessageRight != None):
            # Depending on parameter, we split the messsage.
            #msg_split = msg.split("#")
            # set default values for lower third
            lower_third_1 = 10, 600
            lower_third_2 = 1270, 700
            separator_1 = 280, 610
            separator_2 = 285, 690
            # Set default font for 2 line lower third
            font_right  =  ImageFont.truetype ( fontPath, 36 )
            fontBold2 = ImageFont.truetype (fontPathBold, 36)
            # TODO refactor variable names to fit "dynamic"
            """
            prints bible text for divine service
            msg_split[0] is upper line left
            msg_split[1] is upper line low
            msg_split[2] is bible text, line separation by \n
            """

            #get line counts:

            overlayMessageLeftWrapped  = self.wrapTextDynamically(self.overlayMessageLeft,  19)
            overlayMessageRightWrapped = self.wrapTextDynamically(self.overlayMessageRight, 70)

            line_count = max(len(overlayMessageLeftWrapped.splitlines()), len(overlayMessageRightWrapped.splitlines()))
            lower_third_y = lower_third_1[1]
            # set font and lower third height depending on lines
            if (line_count > 2):
                # 3 lines doesn't change lower third height
                font_right = ImageFont.truetype(fontPath, 26)
                # if 4 or more lines, a higher lower third is needed. With 3 lines,
                # lower third is not affected.
                #FIXME: remove magic numbers
                lower_third_y = 600 - ((line_count - 2) * 26)
                separator_y = 610 - ((line_count - 2) * 26)
                lower_third_1 = 10, lower_third_y
                separator_1 = 280, separator_y
            # Draw lower third
            overlay_image.rectangle((lower_third_1, lower_third_2), fill="white")

            if(self.overlayMessageLeft != ""):
                overlay_image.rectangle((separator_1, separator_2), fill="black")

                #  calculate text positions
                width_left, hight_left =  overlay_image.textsize(overlayMessageLeftWrapped, font=fontBold2)
                if(width_left > 260):
                    # if chapter is too broad, decrease font size
                    fontBold_dyn = ImageFont.truetype(fontPathBold, 26)
                    width_left, hight_left =  overlay_image.textsize(overlayMessageLeftWrapped, font=fontBold_dyn)
                    #wv, hv = overlay_image.textsize(verse, font=fontBold_dyn)
                    overlay_image.multiline_text(((280-width_left)/2, lower_third_y + 10), overlayMessageLeftWrapped, font=fontBold_dyn, fill="black")
                    #overlay_image.text(((280-wv)/2, lower_third_y + 41), verse, font=fontBold_dyn, fill="black")
                else:
                    overlay_image.multiline_text(((280-width_left)/2, lower_third_y + 10), overlayMessageLeftWrapped, font=fontBold2, fill="black")
                    #overlay_image.text(((280-wv)/2, lower_third_y + 46), verse, font=fontBold2, fill="black")


                # right side (if left enabled)
                overlay_image.multiline_text((315, lower_third_y + 10), overlayMessageRightWrapped, font=font_right, fill="black")
            else:
                # right side (if left disabled)
                overlay_image.multiline_text((40, lower_third_y + 10), overlayMessageRightWrapped, font=font_right, fill="black")

        # print logo in upper left
        logoImage = Image.open(logoFile).convert("RGBA")
        logoImage.thumbnail(logo_size)
        overlay.paste(logoImage, (20, 20), logoImage)
        overlay.save(destination_path, "PNG", compress_level=0)


    def generateAndDisplay(self):
        print("Generate Overlay:\n  LeftText:\t {0}\n  RightText:\t {1}\n  Img:\t\t {2}".format(self.overlayMessageLeft, self.overlayMessageRight, self.overlayImage))

        overlayImgNewTmp = tempfile.NamedTemporaryFile(delete=False)

        self.generateOverlay(overlayImgNewTmp)
        self.cR.streamingServer_i.setProperty("imageOverlayLocation", overlayImgNewTmp.name)
        if( self.overlayLastImg != None ):
            os.unlink(self.overlayLastImg)
        self.overlayLastImg = overlayImgNewTmp.name

    # Helper functions for API access
    def generateEndCard(self):
        self.overlayMessageLeft  = ""
        self.overlayMessageRight = "Danke, dass ihr dabei wart.\nDieser Stream ist nun zu Ende."
        self.generateAndDisplay()

    def generateHymn(self, hymn, verse):
        """
        msg is formatted <number>#<verses>, i.e. "123#1-3+5".
        if no verses are given, specify msg as "123#"
        msg_split[0] contains number, msg_split[1] contains verses.
        """

        csvPath = "OverlayGenerator/hymnlist.de.csv"

        # give variables a better name
        hymn_number_text = hymn
        hymn_verses = verse

        # Read hymn list from csv file
        hymns = csv.reader(open(csvPath, "rt"), delimiter=';')
        hymnList = []
        hymnList.extend(hymns)
        names = []
        hymnNumber = int(hymn) - 1 # index starts at 0, hymnal index at 1
        for data in hymnList:
            names.append(data)

        self.overlayMessageLeft  = "Gesangbuch " + str(hymn) + "\n" + str(verse)
        # @FIXME: sanitize max input
        self.overlayMessageRight = names[hymnNumber][1] # get hymn name from list

        self.generateAndDisplay()




    def imageOverlayLocationSanitize(self, s):
        ret = "web/overlayImg/"+sanitize_filename(s)
        try:
            #from https://github.com/ftarlao/check-media-integrity/blob/master/check_mi.py :)
            img = Image.open(ret)  # open the image file
            if(img.format != 'PNG'): #TODO: allow JPG and others
                raise IOError("Image is not a PNG")
            img.verify()  # verify that it is a good image, without decoding it.. quite fast
            img.close()
        except Exception as e:
            er = "ERROR: loaded image {0} not valid".format(ret)

            print(er)
            print(str(e))
            ret = None
            return None, er

        return ret, None

    def setCorsHeaders(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        request.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        request.setHeader('Access-Control-Max-Age', 2520)

    def render_OPTIONS(self, request):
        self.setCorsHeaders(request)
        request.write('')
        request.finish()
        return server.NOT_DONE_YET

    def render_POST(self, request):
        request.setHeader("content-type", 'application/json')
        self.setCorsHeaders(request)

        status={
            'return': {
                'rc':1,
                'msg':"Method not found",
            }
        }

        ##########################################
        # Set Hymn
        ##########################################
        if request.postpath[0]==b'hymn':
            if request.args:
                print("DEPRECATED: use json request instead of form data")
                hymn = request.args[b'hymn'][0].decode()
                verse = request.args[b'verse'][0].decode()
            else:
                # support json content in request
                request_content = json.loads(request.content.read())
                hymn = request_content['hymn']
                verse = request_content['verse']
            status = {
                'return': {
                    'rc':0,
                }
            }
            try:
                hymn = int(hymn)
                if( hymn < 1 or hymn > 438):
                    raise ValueError("Hymn number not in bounds")

                self.generateHymn(hymn, verse)
            except Exception as e:
                status={
                    'return': {
                        'rc':1,
                        'msg':str(e),
                    }
                }




        ##########################################
        # Set Message
        ##########################################
        elif request.postpath[0]==b'overlayText':
            if request.args:
                print("DEPRECATED: use json request instead of form data")
                self.overlayMessageLeft  = request.args[b'left'][0].decode()
                self.overlayMessageRight = request.args[b'right'][0].decode()
            else:
                # support json content in request
                request_content = json.loads(request.content.read())
                self.overlayMessageLeft  = request_content['left']
                self.overlayMessageRight = request_content['right']

            self.generateAndDisplay()
            status = {
                'return': {
                    'rc':0,
                }
            }

        ##########################################
        # Clear Overlay Text
        ##########################################
        elif request.postpath[0]==b'clearOverlayText':
            self.overlayMessageLeft  = None
            self.overlayMessageRight = None
            self.generateAndDisplay()
            status = {
                'return': {
                    'rc':0,
                }
            }

        ##########################################
        # Receive Image Path
        ##########################################
        elif request.postpath[0]==b'overlayImage':
            if request.args:
                print("DEPRECATED: use json request instead of form data")
                path = request.args[b'path'][0].decode()
            else:
                # support json content in request
                request_content = json.loads(request.content.read())
                path = request_content['path']

            self.overlayImage, err = self.imageOverlayLocationSanitize(path)

            if(self.overlayImage != None):
                self.generateAndDisplay()
                status = {
                    'return': {
                        'rc':0,
                    }
                }
            else:

                status = {
                    'return': {
                        'rc':1,
                        'errormsg': err,
                    }
                }


        ##########################################
        # Clear Overlay Image
        ##########################################
        elif request.postpath[0]==b'clearOverlayImage':
            self.overlayImage = None
            self.generateAndDisplay()
            status = {
                'return': {
                    'rc':0,
                }
            }

        # return post
        return str.encode(json.dumps(status))
