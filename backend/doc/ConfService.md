
# Configuration Service

Configuration Service is JSON based REST service.

## Properties

Read and update configuration properties of system.

List of properties:

| Property       | Range                | Unit    | Description |
|----------------|---------------------:|---------|-------------|
| videoBitrate   |  0 - 2000000 (int)   |  bps    | Bitrate of video in bps. |
| volume         | 0.00 - 10.00 (float) |         |            |
| audiodevice    |             (string) |         |            |
| ...            |                      |         |            |

### Endpoints

* Read property `[name]`:

    GET [http://server]/conf?getProperty=[name]

  Result is returned as JSON formatted body:

  ```json
  {
    "result": "value"
  }
  ```

* Set property `[name]` by given `[value]`:

    GET [http://server]/conf?setProperty=[name]&value=[value]

## ROProperties

//@TODO description

### Endpoint

* Read property `[name]`:

    GET [http://server]/conf?getROProperty=

## Status

//@TODO description

| Value | Name    | Description                     |
+-------+--------+----------------------------------+
| 1     | NULL    | //@TODO |
| 2     | READY   | //@TODO |
| 3     | PAUSED  | //@TODO |
| 4     | PLAYING | //@TODO |
| 5     | ERROR   | //@TODO |

### Endpoint

* Read current `status`:

    GET [http://server]/conf?status=

  Result is returned as JSON formatted body:

  ```json
  {
    "status": value
  }
  ```
