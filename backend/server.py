# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0

from twisted.internet import gireactor
import gi
import os
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib, GObject
gireactor.install()

from prometheus_client.twisted import MetricsResource
from twisted.internet import reactor
from twisted.python import log
from twisted.web.static import File
from pprint import pprint
from twisted.web import server, resource

import sys
#import _thread
import socket
import json
import time
from enum import Enum, auto
from PIL import Image as Image # used for png validity check
from pathvalidate import sanitize_filename

import StreamingServer
from WebAPI import WebAPI
from OverlayGenerator import OverlayGenerator
import ClassRegistry

class BaseResource(resource.Resource):
    def setCorsHeaders(self, request):
        request.setHeader('Access-Control-Allow-Origin', '*')
        request.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        request.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        request.setHeader('Access-Control-Max-Age', 2520)

    def render_OPTIONS(self, request):
        self.setCorsHeaders(request)
        request.write('')
        request.finish()
        return server.NOT_DONE_YET

class Home(BaseResource):
    isLeaf = False

    def __init__(self, cR):
        self.cR = cR

    def getChild(self, name, request):
        if name == '':
            return self
        return resource.Resource.getChild(self, name, request)

    def render_GET(self, request):
        args = request.args
        print(args)
        self.setCorsHeaders(request)
        request.setHeader("content-type", 'application/json')

        if b'streaming' in args:
            val = args[b'streaming'][0]
            print("val ", val)
            if val == b'start':
                print("start pipeline")
                self.cR.streamingServer_i.start_pipeline_user(False)
            elif val == b'test':
                print("start test")
                self.cR.streamingServer_i.start_pipeline_user(False, test = True)
            elif val == b'receive':
                print("start receive pipeline")
                self.cR.streamingServer_i.start_pipeline_user(True)
            elif val == b'stop':
                print("stop pipeline")
                self.cR.streamingServer_i.stop_pipeline_user()

        elif b'status' in args:

            status = {'status': self.cR.streamingServer_i.state}
            return str.encode(json.dumps(status))

        elif b'getROProperties' in args:
            status = {'audiodevices': self.cR.streamingServer_i.get_audio_device_names(True),
                      'audio_sink_devices': self.cR.streamingServer_i.get_audio_device_names(False),
                      'hostname': self.cR.streamingServer_i.hostname}
            return str.encode(json.dumps(status))

        elif b'getProperty' in args:

            propertyName = args[b'getProperty'][0].decode()
            print('get propertyName: "%s"' % propertyName)

            status = {'return': self.cR.streamingServer_i.getProperty(propertyName)}
            return str.encode(json.dumps(status))

        elif b'setProperty' in args:
            if not b'value' in args:
                print("setproperty value not set")
                status = {'return': False}
                return str.encode(json.dumps(status))

            propertyName = args[b'setProperty'][0].decode()
            value = args[b'value'][0].decode()
            print('set propertyName: "%s" value: "%s"' % (propertyName, value))
            #!FIXME: check on user input and serialize it

            status = {'return': self.cR.streamingServer_i.setProperty(propertyName, value)}
            return str.encode(json.dumps(status))



        status = {'return': 'ok'}
        return str.encode(json.dumps(status))

class Power(BaseResource):
    isLeaf = True

    def __init__(self):
        pass

    def getChild(self, name, request):
        if name == '':
            return self
        return resource.Resource.getChild(self, name, request)

    def render_GET(self, request):
        self.setCorsHeaders(request)
        request.setHeader("content-type", 'application/json')

        status={
            "rc":42,
            "msg":"Unknown Method"
        }
        if request.postpath[0]==b'off':
            print("POWEROFF Requested")
            # status= {
            #     "rc":0,
            #     "msg":"Poweroff OK"
            # }
            os.system('sudo shutdown -h 0')
        elif request.postpath[0]==b'reboot':
            # status= {
            #     "rc":0,
            #     "msg":"Reboot OK"
            # }
            print("REBOOT REQUESTED")
            os.system('sudo reboot "3 tryboot"')


        return str.encode(json.dumps(status))


class OverlayPictures(BaseResource):
    isLeaf = True
    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    def render_POST(self,request):
        self.setCorsHeaders(request)
        request.setHeader("content-type", 'application/json')

        if request.postpath[0]==b'upload':
            imageFileName = "web/overlayImg/"+sanitize_filename(request.args[b'filename'][0].decode("utf-8"))
            try:
                outputStream = open(imageFileName, 'wb')
                outputStream.write(request.args[b'file'][0])
                outputStream.close()
                #from https://github.com/ftarlao/check-media-integrity/blob/master/check_mi.py :)
                img = Image.open(imageFileName)  # open the image file
                if(img.format != 'PNG'):
                    raise IOError("Image is not a PNG")
                img.verify()  # verify that it is a good image, without decoding it.. quite fast
                img.close()
                status={
                    'return': {
                        'rc':0,
                        'msg':"Upload OK",
                    }
                }
            except Exception as e:
                try:
                    os.remove(imageFileName)
                except:
                    pass
                status={
                    'return': {
                        'rc':42,
                        'msg':str(e),
                    }
                }

            return str.encode(json.dumps(status))

        elif request.postpath[0]==b'delete':
            files=json.loads(request.args[b'files'][0].decode("utf-8"))
            print(files)
            status={
                'return': {
                    'rc':0,
                    'msg':"Files deleted",
                }
            }
            try:
                for file in files:
                    filename = sanitize_filename(file["filename"])
                    if(filename == "abendmahl.png"):
                        raise IOError("cant't delete default image abendmahl.png")

                    os.remove("web/overlayImg/"+filename)

            except Exception as e:
                status={
                    'return': {
                        'rc':1,
                        'msg':str(e),
                    }
                }
            return str.encode(json.dumps(status))

        return str.encode(json.dumps({"msg":"Method not found"}))

    def render_GET(self, request):
        self.setCorsHeaders(request)
        request.setHeader("content-type", 'application/json')

        if request.postpath[0]==b'getAll':
            print("getOverlayPictures called")
            pictures=[]
            for root, dirs, files in os.walk("web/overlayImg"):
                for filename in files:
                    fixed=False
                    if(filename=="abendmahl.png" or filename=="schmuck.png"):
                        fixed=True
                    pictures.append({
                        'filename':filename,
                        'fixed':fixed
                    })
            status = {
                'return': {
                    'msg':"OK",
                    'data': {
                        'pictures': pictures,
                    }
                }
            }
            return str.encode(json.dumps(status))

        return str.encode(json.dumps({"msg":"Method not found"}))

class WebReactorNoConfig(BaseResource):
    isLeaf = True

    def render_GET(self, request):
        request.setHeader(b"content-type", b"text/plain")
        self.setCorsHeaders(request)
        content = u"This streaming-server is not configured correctly. Please, place config file in /boot."
        return content.encode("ascii")

class FileCustomCaching(File):
    def render(self,request):
        if request.path in [b"/", b"/index.html"]:
            request.setHeader(b"Cache-Control", b"no-cache, no-store, must-revalidate")
        return super(FileCustomCaching, self).render(request)

if __name__ == "__main__":
    Gst.init(None)
    Gst.debug_set_active(True)
    Gst.debug_set_default_threshold(1)
    print( socket.gethostname() )

    cR = ClassRegistry.ClassRegistry()

    if ( socket.gethostname() == 'pi-baseimage'):
        print('ERROR: the image is not yet configured')
        site = server.Site(WebReactorNoConfig())
    else:
        streamingS = StreamingServer.StreamingServer(cR)
        GLib.timeout_add_seconds(5, streamingS.publicateMetrics)

        root = FileCustomCaching('web')
        root.putChild(b"conf", Home(cR))
        root.putChild(b"APIv1", WebAPI(cR))
        root.putChild(b"overlayGenerator", OverlayGenerator(cR))
        root.putChild(b"overlayPictures", OverlayPictures())
        root.putChild(b"power", Power())
        root.putChild(b'metrics', MetricsResource())

        site = server.Site(root)

    PORT = 8080

    reactor.listenTCP(PORT, site)
    print('Started webserver at port %d' % PORT)

    reactor.run()
    # reactor.registerGApplication(app)
    # GObject.threads_init()

    # try:
    #    _thread.start_new_thread( reactor.run() )
    # except:
    #    print ("Error: unable to start thread")

    print("GLIB MainLoop")

    #loop = GLib.MainLoop()
    # loop.run()

    print("run")
