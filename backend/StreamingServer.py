# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib, GObject


import prometheus_client
import os
import time
import socket
import configparser
import requests
from enum import Enum, auto, IntEnum

PIPELINE_PARTS = {
    'LIVEAUDIOSRC': 'alsasrc name=audiosrc',

    'TESTAUDIOSRC': 'audiotestsrc wave=ticks',

    'GL': " queue min-threshold-buffers=1 ! glupload ! glcolorconvert ! gloverlay overlay-width=1280 overlay-height=720 name=imageOverlay alpha=1 location=OverlayGenerator/default.png ! tee name=gltee ! glcolorconvert ! gldownload ! video/x-raw,format=YUY2",

    'GL_400': "queue ! glupload ! glcolorconvert ! gloverlay overlay-height=400 overlay-width=704 name=imageOverlay alpha=1 location=OverlayGenerator/default.png ! tee name=gltee ! glcolorconvert ! gldownload ! video/x-raw,format=YUY2",

    'GL_SINK': "gltee. ! queue min-threshold-buffers=1 leaky=downstream ! glimagesink sync=false",

    'JANUSUPLOAD': '''vid. ! queue  name=udpsink_start ! rtph264pay ! udpsink host=127.0.0.1 port=8004 name=udpsink
        audio. ! queue ! opusenc bitrate=48000 ! rtpopuspay ! udpsink host=127.0.0.1 port=8005 name=udpsink_audio''',

    'KMSSINK': "videoUncompressed. ! queue ! videoconvert ! kmssink",

    'RTMPSINK': "mux. ! queue2 name=sinkQueue ! rtmpsink name=rtmpsink",

    'TEXTOVERLAY': "textoverlay valignment=bottom halignment=center line-alignment=left font-desc=\"Sans, 28\" name=txt shaded-background=yes",

    #first queue for CAMLINK 1080: buffers=3 otherwise 1
    'H264FLV': "queue min-threshold-buffers=1 ! v4l2h264enc output-io-mode=4 capture-io-mode=4 extra-controls=\"controls,h264_profile=3,video_bitrate=1500000,h264_i_frame_period=100,video_bitrate_mode=0,repeat_sequence_header=1;\" name=encoder ! video/x-h264,profile=high,level=(string)4 ! tee name=vid ! queue name=rtmpsink_start ! h264parse ! flvmux name=mux streamable=true latency=500000000 min-upstream-latency=500000000", # metadatacreator=\"Hai-End Streaming Server\"

    #last queue for CAMLINK 1080:           time=500000000 buffer=3
    #last queue for MACROSILICON 1080:      time=100000000 buffer=3
    'AUDIOENC': "queue ! audioresample ! audio/x-raw,rate=48000 ! volume volume=1.8 name=volume !  queue ! tee name=audio ! queue ! audioconvert ! avenc_aac aac-coder=twoloop bitrate=128000 ! queue ! mux.",
}

#
PIPELINE_DESC_ANALOG = '''
    v4l2src name=videosrc extra-controls="c,volume=46028" pixel-aspect-ratio=1/1 ! video/x-raw,format=YUY2,width=720,height=557,framerate=25/1,interlace-mode=interleaved ! queue ! videoconvert ! deinterlace method=vfir ! queue ! videorate ! video/x-raw,framerate=25/1 ! queue ! videocrop top=79 bottom=78 left=8 right=8 ! queue ! frei0r-filter-white-balance neutral-color-r=1 neutral-color-g=1 neutral-color-b=1 name=wb ! queue ! frei0r-filter-hqdn3d spatial=0.2 temporal=0.2 ! queue ! frei0r-filter-sharpness size=0.1 amount=0.3 ! {GL_400} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)


PIPELINE_DESC_DIGITAL = '''
    v4l2src name=videosrc ! video/x-raw,width=1280,height=720,framerate=25/1 ! queue ! videorate max-rate=25 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)


#YUY2, NV12
PIPELINE_DESC_CAMLINK = '''
    v4l2src name=videosrc io-mode=4 ! v4l2convert output-io-mode=4 capture-io-mode=4  ! videorate max-rate=25 ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)

# Macrosilicon USB 2.0 HDMI Capture Device
# Capable of 720p10 YUV and 720p60, 720p50, 720p30, 720p20, 720p10 MJPEG
#v4l2src name=videosrc  ! image/jpeg,width=1280,height=720,framerate=50/1 ! queue ! videorate max-rate=25 ! queue ! jpegdec ! queue ! videoconvert ! queue ! videoscale ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
PIPELINE_DESC_MACROSILICON2109 = '''
    v4l2src name=videosrc io-mode=4 pixel-aspect-ratio=1/1 ! image/jpeg,width=1280,height=720,framerate=50/1 ! videorate max-rate=25 ! queue ! jpegdec idct-method=0 !  {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)

PIPELINE_DESC_ATEMMINIPRO = '''
    v4l2src name=videosrc  ! image/jpeg ! queue ! videorate max-rate=25 ! queue ! jpegdec ! queue ! videoconvert ! queue ! videoscale ! video/x-raw,width=1280,height=720 ! {GL} ! {H264FLV}
    {LIVEAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)

PIPELINE_DESC_TEST = '''
    videotestsrc ! video/x-raw,width=1280,height=720,framerate=25/1 ! {GL} ! {H264FLV}
    {TESTAUDIOSRC} ! {AUDIOENC}
    {RTMPSINK}
    {JANUSUPLOAD}
    {GL_SINK}
'''.format(**PIPELINE_PARTS)

PIPELINE_DESC_RECEIVE = '''
    souphttpsrc name=videosrc is-live=true location=https://out1.nac-cdn.net/testvideo/720p25/teststream_720mid/index.m3u8 ! hlsdemux ! queue ! tsdemux name=mux ! tee name=vid ! queue ! h264parse ! queue min-threshold-buffers=3 ! v4l2h264dec ! queue min-threshold-buffers=3 ! glupload ! glcolorconvert ! glcolorscale ! video/x-raw(memory:GLMemory),width=1280,height=720 ! glimagesinkelement sync=true
    mux. ! queue ! aacparse ! avdec_aac ! tee name=audio ! queue ! audioconvert ! alsasink device=hw:0 name=audiosink
    vid. ! queue  name=udpsink_start ! video/x-h264,stream-format=(string)byte-stream,alignment=(string)au ! rtph264pay ! udpsink host=127.0.0.1 port=8004 name=udpsink
    audio. ! queue ! audioconvert ! opusenc bitrate=48000 ! rtpopuspay ! udpsink host=127.0.0.1 port=8005 name=udpsink_audio
'''.format(**PIPELINE_PARTS)

def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def remove_postfix(text, postfix):
    if text.endswith(postfix):
        return text[:-len(postfix)]
    return text

class videoDeviceModelEnum(Enum):
    TEST = "TEST"
    HAUPPAUGE = "HAUPPAUGE"
    RASPICAM = "RASPICAM"
    CAMLINK = "CAMLINK"
    AUVIDEA = "AUVIDEA"
    MACROSILICON2109 = "MACROSILICON2109"
    ATEMMINIPRO = "ATEMMINIPRO"

class errorStatesEnum(IntEnum):
    NULL = 1
    READY = 2
    PAUSED = 3
    PLAYING = 4
    ERROR = 5

class StreamingServer:
    def __init__(self, cR):
        self.cR = cR
        self.cR.setStreamingServer_i(self)

        self.pipe = None
        self.properties = dict()
        self.receivePipe = False

        self.status = "STOPPED"
        self.state = errorStatesEnum.NULL
        self.state_intended = errorStatesEnum.NULL
        self.hostname = remove_prefix(socket.gethostname(), 'pi-')
        self.fqdn = socket.getfqdn()

        # Read Config
        self.configGlobal = configparser.ConfigParser()
        self.configGlobal.read('../streaming-server-global.conf')
        self.configLocal = configparser.ConfigParser()
        self.configLocal.read('../streaming-server-local.conf')

        if not 'properties' in self.configLocal:
            self.configLocal['properties'] = {}
        if not 'audiodevice' in self.configLocal['properties']:
            self.configLocal['properties']['audiodevice'] = ""
        if not 'audio_sink_device' in self.configLocal['properties']:
            self.configLocal['properties']['audio_sink_device'] = ""
        if not 'stream' in self.configLocal:
            self.configLocal['stream'] = {}

        self.registerProperty("videoBitrate",           None,   "extra-controls",   True,   lambda f: self.setVideoBitrateFromExtraControls(f), lambda s: self.getVideoBitrateFromExtraControls(s))
        self.registerProperty("extra-controls",         None,   "extra-controls",   False,  None, None)
        self.registerProperty("volume",                 None,   "volume",           True,   lambda f: float(f),                         None)
        self.registerProperty("rtmpLocation",           None,   "location",         False,  None,                                       None)
        self.registerProperty("wbRed",                  None,   "neutral-color-r",  True,   lambda f: float(f),                         None)
        self.registerProperty("wbBlue",                 None,   "neutral-color-b",  True,   lambda f: float(f),                         None)
        self.registerProperty("wbGreen",                None,   "neutral-color-g",  True,   lambda f: float(f),                         None)
        self.registerProperty("imageOverlayAlpha",      None,   "alpha",            False,  lambda f: float(f),                         None)
        self.registerProperty("imageOverlayLocation",   None,   "location",         False,  None,   None)
        self.registerProperty("audiodevice",            None,   "device",           True,   lambda f: self.get_audio_device_dev_by_name(f, True),   lambda f: self.get_audio_device_name_by_dev(f, True))
        self.registerProperty("audio_sink_device",      None,   "device",           True,   lambda f: self.get_audio_device_dev_by_name(f, False),  lambda f: self.get_audio_device_name_by_dev(f, False))

        #register metrics
        self.metricsBitrate = prometheus_client.Gauge('streaming_bitrate', 'Avarage outgoing bitrate in bits/s')
        self.metricsRunningState = prometheus_client.Gauge('streaming_state', 'Streaming Status', ['state'])
        self.metricsRunningState.labels('state').set_function(lambda: self.state)
        self.metricsRunningState.labels('intended').set_function(lambda: self.state_intended)

        self.git_version = "1.0-rc1"
        i = prometheus_client.Info('streaming_server', 'streaming-server info')
        i.info({'version': self.git_version})

        self.metricsQueue = None

        #restarting logic
        self.restartingTime = 0
        self.restartingNumbers = 0
        self.restartingDoing = False

        #configure request for cloud API
        self.cloudRequest = requests.Session()
        if( self.configGlobal.get('cloud', 'cloud_proxy', fallback="") != "" ):
            self.cloudRequest.proxies.update({'http': self.configGlobal.get('cloud', 'cloud_proxy')})


        #test if the pipeline was started before thus we can recover from a crash
        streamStarted = self.configLocal.get('stream', 'started', fallback="false")
        self.receivePipe = self.configLocal.getboolean('stream', 'receivePipe', fallback=False)
        if self.configGlobal.get('device', 'type', fallback="spare") == "spare":
            if( streamStarted == "true" ):
                self.start_pipeline(receivePipe = self.receivePipe)
        else:
            r =  self.cloudRequest.post(self.configGlobal['cloud']['keypress_url'], data = {'key': 'recover', 'streamStarted': streamStarted, 'streamReceived': self.receivePipe})
            print(r.json())


    def getVideoBitrateFromExtraControls(self, s):
        if s == None:
            return s
        if type(s) == str:
            return s
        return s.get_int('video_bitrate').value

    def setVideoBitrateFromExtraControls(self, s):
        extraControls = self.getProperty('extra-controls')
        print(extraControls)
        if (extraControls == "false"):
            return None
        extraControls.set_value('video_bitrate', int(s))
        print(extraControls)

        return extraControls

    '''
    AUDIO DEVICE SECTION
    '''

    def get_audio_devices(self, is_src_device : bool):
        # ALSA dependency here
        # Find a suitable device string for alsasrc
        if is_src_device == True:
            devices = os.popen("arecord -l")
        else:
            devices = os.popen("aplay -l")
        device_strings = devices.read().split('\n')
        # The entry for a capture device starts with 'card n' where n is an integer
        # The syntax for refering to the capture device is 'hw:n' with the n from above
        devicelist = [('hw:' + s[len("card "):]).partition(": ")[0::2] for s in device_strings if s.startswith("card ")]
        return devicelist

    def get_audio_device_names(self, is_src_device : bool):
        return [s[1] for s in self.get_audio_devices(is_src_device)]

    def get_audio_device_dev_by_name(self, name, is_src_device : bool):
        ret = [a[0] for a in self.get_audio_devices(is_src_device) if a[1] == name]
        # Should get exactly one match
        if not len(ret):
            print('Warning: No matching audio {0} device, using one of the available devices according to arcane rules'.format('src' if is_src_device == True else 'sink'))
            return self.get_audio_device(is_src_device)
        if len(ret) > 1:
            print('Warning: More than one audio {0} device matched the selected string, using first match'.format('src' if is_src_device == True else 'sink'))
        return ret[0]

    def get_audio_device_name_by_dev(self, dev, is_src_device : bool):
        ret = [a[1] for a in self.get_audio_devices(is_src_device) if a[0] == dev]
        # Should get exactly one match
        if not len(ret):
            print('Warning: No matching audio {0} device, using one of the available devices according to arcane rules'.format('src' if is_src_device == True else 'sink'))
            # FIXME: This is wrong and should never happen unless no device is selected or a device disappeared
            return ""
        if len(ret) > 1:
            print('Warning: More than one audio {0} device matched the selected string, using first match'.format('src' if is_src_device == True else 'sink'))
        return ret[0]

    def get_audio_device(self, is_src_device : bool):
        # Pick the first audio input device
        devs = self.get_audio_devices(is_src_device)
        if devs:
            return devs[0][0]
        return ""

    '''
    VIDEO DEVICE SECTION
    '''

    def getvideodevice(self):
        #return videoDeviceModelEnum.TEST, ""
        devices = os.popen("v4l2-ctl --list-devices")
        device_string = devices.read()
        device_string = device_string.split("\n")
        for x in range(len(device_string)):
            try:
                videoDevice = device_string[x+1].strip()
            except:
                return videoDeviceModelEnum.TEST, ""
            if(device_string[x].find("Hauppauge USB Live 2") != -1):
                return videoDeviceModelEnum.HAUPPAUGE, videoDevice
            elif(device_string[x].find("mmal service 16.1 (platform:bcm2835-v4l2") != -1):
                return videoDeviceModelEnum.RASPICAM, videoDevice
            elif(device_string[x].find("unicam (platform:fe801000.csi))") != -1):
                return videoDeviceModelEnum.AUVIDEA, videoDevice
            elif(device_string[x].find("Cam Link 4K: Cam Link 4K") != -1):
                return videoDeviceModelEnum.CAMLINK, videoDevice
            elif(device_string[x].find("534d:2109") != -1):
                # Macrosilicon USB 2.0 HDMI Capture Device
                return videoDeviceModelEnum.MACROSILICON2109, videoDevice
            elif(device_string[x].find("USB3.0 HD VIDEO") != -1):
                # Macrosilicon USB 2.0 HDMI Capture Device
                return videoDeviceModelEnum.MACROSILICON2109, videoDevice
            elif(device_string[x].find("USB Video: USB Video") != -1):
                # Macrosilicon USB 2.0 HDMI Capture Device
                return videoDeviceModelEnum.MACROSILICON2109, videoDevice
            elif(device_string[x].find("USB3. 0 capture: USB3. 0 captur") != -1):
                # Macrosilicon USB 3.0 HDMI Capture Device
                return videoDeviceModelEnum.MACROSILICON2109, videoDevice
            elif(device_string[x].find("Blackmagic Design") != -1):
                return videoDeviceModelEnum.ATEMMINIPRO, videoDevice
        return videoDeviceModelEnum.TEST, ""

    #differentiate between spare/local PIs and cloud PIs
    def start_pipeline_user(self, recievePipe, test = False):
        if self.configGlobal.get('device', 'type', fallback="spare") == "spare":
            self.start_pipeline(None, recievePipe)
        else:
            if(recievePipe == True):
                keyPressed = 'start_receive'
            else:
                if(test == False):
                    keyPressed = 'start_stream'
                else:
                    keyPressed = 'start_test'
            r =  self.cloudRequest.post(self.configGlobal['cloud']['keypress_url'], data = {'key': keyPressed})
            print(r.json())

    def start_pipeline(self, stream_url = None, receivePipe = False):
        if self.state_intended == errorStatesEnum.NULL:
            if receivePipe == False:
                videoDeviceModel, videoDev = self.getvideodevice()
                audiodev = self.get_audio_device(True)

                if( (videoDev == videoDeviceModelEnum.TEST) | (audiodev == "")):
                    print("ERROR: no also or video device found -> teststream")
                    print("videoDev: ", videoDev)
                    print("audiodev", audiodev)
                    videoDeviceModel = videoDeviceModelEnum.TEST
                    audiodev = ""

                #forcing the pipe if the config is set to
                force_pipe = self.configLocal.get('stream', 'force_pipe', fallback="None")
                if force_pipe != "None":
                    print("Forcing the video pipe to %s" % force_pipe)
                    videoDeviceModel = videoDeviceModelEnum[force_pipe]

                #choose the actually used pipe (string representation)
                if videoDeviceModel == videoDeviceModelEnum.HAUPPAUGE:
                    usedPipe = PIPELINE_DESC_ANALOG
                elif (videoDeviceModel == videoDeviceModelEnum.RASPICAM) | (videoDeviceModel == videoDeviceModelEnum.AUVIDEA):
                    usedPipe = PIPELINE_DESC_DIGITAL
                elif videoDeviceModel == videoDeviceModelEnum.CAMLINK:
                    usedPipe = PIPELINE_DESC_CAMLINK
                elif videoDeviceModel == videoDeviceModelEnum.ATEMMINIPRO:
                    usedPipe = PIPELINE_DESC_ATEMMINIPRO
                elif videoDeviceModel == videoDeviceModelEnum.MACROSILICON2109:
                    usedPipe = PIPELINE_DESC_MACROSILICON2109
                elif videoDeviceModel == videoDeviceModelEnum.TEST:
                    print('Test video device selected')
                    usedPipe = PIPELINE_DESC_TEST
                else:
                    print('ERROR: Unknown video device type')
                    usedPipe = PIPELINE_DESC_TEST

                print("Used Pipe: {}".format(usedPipe))
                self.pipe = Gst.parse_launch(usedPipe)

                self.setPropertyObjAndLoadPersistence("extra-controls",         self.pipe.get_by_name('encoder') );
                self.setPropertyObjAndLoadPersistence("videoBitrate",           self.pipe.get_by_name('encoder') );
                self.setPropertyObjAndLoadPersistence("volume",                 self.pipe.get_by_name('volume') );
                self.setPropertyObjAndLoadPersistence("rtmpLocation",           self.pipe.get_by_name('rtmpsink') );
                self.setPropertyObjAndLoadPersistence("wbRed",                  self.pipe.get_by_name('wb') );
                self.setPropertyObjAndLoadPersistence("wbBlue",                 self.pipe.get_by_name('wb') );
                self.setPropertyObjAndLoadPersistence("wbGreen",                self.pipe.get_by_name('wb') );
                self.setPropertyObjAndLoadPersistence("imageOverlayAlpha",      self.pipe.get_by_name('imageOverlay') );
                self.setPropertyObjAndLoadPersistence("imageOverlayLocation",   self.pipe.get_by_name('imageOverlay') );
                self.setPropertyObjAndLoadPersistence("audiodevice",            self.pipe.get_by_name('audiosrc') );

                self.metricsQueue = self.pipe.get_by_name("sinkQueue")

                # setting the rtmp streaming key/location
                locationAdd = ""
                if videoDeviceModel == videoDeviceModelEnum.HAUPPAUGE:
                    locationAdd = "43"
                if (stream_url == None):
                    location = self.configGlobal['stream']['url'] + locationAdd + '/' + \
                        self.hostname + self.configGlobal['stream']['key']
                else:
                    location = stream_url
                #self.rtmpsink.set_property("location", location)
                self.setProperty("rtmpLocation", location)
                print("set rtmp location to: ", location)

                # print the audio source device
                audiosrc = self.pipe.get_by_name('audiosrc')
                if audiosrc != None:
                    audiodev = audiosrc.get_property("device")
                    print("using audio src device: " + audiodev)


                #setting the video src device
                videosrc = self.pipe.get_by_name('videosrc')
                if videosrc != None:
                    print("using video device: " + videoDev)
                    print("video type: " + videoDeviceModel.value)
                    videosrc.set_property("device", videoDev)

            else: #Receive Pipe
                usedPipe = PIPELINE_DESC_RECEIVE

                print("Used Pipe: {}".format(usedPipe))
                self.pipe = Gst.parse_launch(usedPipe)


                self.setPropertyObjAndLoadPersistence("rtmpLocation",           self.pipe.get_by_name('videosrc') );
                self.setPropertyObjAndLoadPersistence("audio_sink_device",      self.pipe.get_by_name('audiosink') );

                if stream_url != None:
                    self.setProperty("rtmpLocation", stream_url)
                    print("set rtmp location to: ", stream_url)
                #self.metricsQueue = self.pipe.get_by_name("sinkQueue")

                # print the audio sink device
                audiosrc = self.pipe.get_by_name('audiosink')
                if audiosrc != None:
                    audiodev = audiosrc.get_property("device")
                    print("using audio sink device: " + audiodev)

            bus = self.pipe.get_bus()
            bus.add_signal_watch()
            bus.connect("message", self.on_message)

            self.pipe.set_state(Gst.State.PLAYING)
            self.state_intended = errorStatesEnum.PLAYING

            self.receivePipe = receivePipe
            self.configLocal.set('stream', 'started', "true")
            self.configLocal.set('stream', 'receivePipe', str(self.receivePipe))
            self.configLocalWrite()


    #differentiate between spare/local PIs and cloud PIs
    def stop_pipeline_user(self):
        if self.configGlobal.get('device', 'type', fallback="spare") == "spare":
            self.stop_pipeline()
        else:
            if(self.receivePipe == True):
                keyPressed = 'stop_receive'
            else:
                keyPressed = 'stop_stream'
            r =  self.cloudRequest.post(self.configGlobal['cloud']['keypress_url'], data = {'key': keyPressed})
            print(r.json())
            self.stop_pipeline()

    def stop_pipeline(self):
        if self.state_intended != errorStatesEnum.NULL:
            self.cR.overlayGen_i.generateEndCard()
            time.sleep(2)

            self.pipe.set_state(Gst.State.NULL)
            self.state = errorStatesEnum.NULL
            self.state_intended = errorStatesEnum.NULL
            self.deregisterAllPropertyObj()
            self.pipe = None
            self.metricsQueue = None
            self.metricsBitrate.set(0)

            self.configLocal.set('stream', 'started', "false")
            self.configLocalWrite()

    def updateStreamLocation(self, url):
        # @TODO: we might need a more sophisticated method here (stop the rtmpsink only rather then the whole pipe)
        ret = False
        if self.state_intended == errorStatesEnum.PLAYING:
            self.pipe.set_state(Gst.State.NULL)
            self.setProperty("rtmpLocation", url)
            self.pipe.set_state(Gst.State.PLAYING)
            ret = True

        return ret

    def setPropertyObjAndLoadPersistence(self, propertyName, obj):

        if (not propertyName in self.properties):
            print("Error: property ", propertyName, " not available in Properties")
            return

        self.properties[propertyName]['obj'] = obj

        if( self.properties[propertyName]['localConfigPersistent'] == True ):
            if 'properties' in self.configLocal:
                if propertyName in self.configLocal['properties']:
                    self.setProperty(propertyName, self.configLocal['properties'][propertyName], False)

    def registerProperty(self, propertyName, obj, gstPropertyName, localConfigPersistent, valueLambda, valueReverseLambda):
        if( (propertyName == "") | (gstPropertyName == "") ):
            print("Error: registerProperty false input")
            print(propertyName)
            print(obj)
            print(gstPropertyName)
            return

        print("registerProperty: ", propertyName, " ", gstPropertyName, " ", type(obj), type(valueLambda), type(valueReverseLambda))
        self.properties[propertyName] = {'obj': obj, 'gstPropertyName': gstPropertyName, 'localConfigPersistent': localConfigPersistent, 'valueLambda': valueLambda, 'valueReverseLambda': valueReverseLambda}


    def deregisterAllPropertyObj(self):
        print("derigster all objects in properties")
        for _,p in self.properties.items():
            p['obj'] = None

    def configLocalWrite(self):
        with open('../streaming-server-local.conf', 'w') as configLocalFile:
                self.configLocal.write(configLocalFile)

    def setProperty(self, propertyName, value, write=True):
        if not ( (propertyName in self.properties) ):
            print("Error: requested property not registered")
            return "false"


        #safe the property in the config file
        if( (self.properties[propertyName]['localConfigPersistent'] == True) & (write == True) ):
            self.configLocal['properties'][propertyName] = value
            self.configLocalWrite()

        #set the property on a running stream if the stream is currently active
        if (self.properties[propertyName]['obj'] != None):
            #call the valueLambda function on the actual value
            if (self.properties[propertyName]['valueLambda'] != None):
                value = self.properties[propertyName]['valueLambda'](value)

            #set property
            obj = self.properties[propertyName]['obj']
            gstPropertyName = self.properties[propertyName]['gstPropertyName']
            obj.set_property(gstPropertyName, value)
            return "true"

        return "true";


    def getProperty(self, propertyName):

        ret = "false"
        # first try to get the value from the acutal stream
        # if this is not successfull then try to get it from the persistence saves
        if not ( (propertyName in self.properties) ):
            print("Error: requested property not registered")
        elif( self.properties[propertyName]['obj'] != None ):
            obj = self.properties[propertyName]['obj']
            gstPropertyName = self.properties[propertyName]['gstPropertyName']
            ret = obj.get_property(gstPropertyName)
            #call the valueReverseLambda function on the actual value
            if (self.properties[propertyName]['valueReverseLambda'] != None):
                ret = self.properties[propertyName]['valueReverseLambda'](ret)
        elif( self.properties[propertyName]['localConfigPersistent'] == True ):
            ret = self.configLocal.get('properties',propertyName,fallback="false")
            #call the valueReverseLambda function on the actual value
            if (self.properties[propertyName]['valueReverseLambda'] != None):
                ret = self.properties[propertyName]['valueReverseLambda'](ret)

        return ret

    def publicateMetrics(self):
        if self.metricsQueue != None:
            q = self.metricsQueue.get_property("avg-in-rate")
            self.metricsBitrate.set(q*8)
            #print("publicate metrics")
            #print(q)

        return True

    def on_message(self, bus, message):
        t = message.type

        if t == Gst.MessageType.EOS:
            # self.player.set_state(Gst.State.NULL)
            print("EOS")
        elif t == Gst.MessageType.INFO:
            print("Info: ", message.parse())
        elif t == Gst.MessageType.ERROR:
            # self.player.set_state(Gst.State.NULL)
            self.state = errorStatesEnum.ERROR
            err, debug = message.parse_error()
            print("Error: ", err, debug)

        elif t == Gst.MessageType.STATE_CHANGED:
            oldSate, newState, _ = message.parse_state_changed()
            print("StateChange: ", oldSate.value_nick, " -> ",
                  newState.value_nick, "\t", message.src.get_name())
            if( message.src.get_name().startswith("pipeline") ):
                if(newState.value_nick == "null"):
                    self.state = errorStatesEnum.NULL
                elif(newState.value_nick == "ready"):
                    self.state = errorStatesEnum.READY
                elif(newState.value_nick == "paused"):
                    self.state = errorStatesEnum.PAUSED
                elif(newState.value_nick == "playing"):
                    self.state = errorStatesEnum.PLAYING
                else:
                    self.state = errorStatesEnum.ERROR
            #print( repr(message) )
        #elif t == Gst.MessageType.QOS:
        #    print('Qos Message:', message.parse_qos())
        #    return
        #else:
        #   print(t)
