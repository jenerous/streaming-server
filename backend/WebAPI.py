# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0

from twisted.web import resource
import json


class WebAPI(resource.Resource):
    isLeaf = True
    cR = None

    def __init__(self, cR):
        self.cR = cR

    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    def render_GET(self, request):
        request.setHeader("content-type", 'application/json')
        request.setHeader("Access-Control-Allow-Origin", '*')
        status={
            'return': {
                'rc':1,
                'msg':"Method not found",
            }
        }

        #########################################
        # Status
        #########################################
        if request.postpath[0]==b'status':
            print("APIv1 get status")
            

            videoDeviceModel, _ = self.cR.streamingServer_i.getvideodevice()
            audioDevices = self.cR.streamingServer_i.get_audio_device_names(True)
            AudioSelectedDevice = self.cR.streamingServer_i.getProperty("audiodevice")

            status = {
                'return': {
                    'rc':0,
                    'msg':"OK"
                },
                'status': {
                    'version': self.cR.streamingServer_i.git_version,
                    'stream_status': self.cR.streamingServer_i.state,
                    'stream_status_intended': self.cR.streamingServer_i.state_intended,
                    'peripherals_power': False,
                    'video_device_model': videoDeviceModel.value,
                    'audio_devices': audioDevices,
                    'audio_selected_device': AudioSelectedDevice,
                    'stream_location': self.cR.streamingServer_i.getProperty("rtmpLocation")
                } 
            }
        
        #return get
        return str.encode(json.dumps(status))



    def render_POST(self,request):
        request.setHeader("content-type", 'application/json')
        request.setHeader("Access-Control-Allow-Origin", '*')
        status={
            'return': {
                'rc':1,
                'msg':"Method not found",
            }
        }

        
        #########################################
        # Show Message to User 
        #########################################
        if request.postpath[0]==b'show_message':
            print("APIv1 show message")
            msg = request.args[b'msg'][0].decode()

            print("Message to display {0}".format(msg))
            #@todo: to something meaningful
            status = {
                'return': {
                    'rc':0,
                } 
            }

        #########################################
        # Start Stream
        #########################################
        elif request.postpath[0]==b'start_stream':
            print("APIv1 start stream")
            streamURL = request.args[b'url'][0].decode()

            self.cR.streamingServer_i.start_pipeline(streamURL)
            status = {
                'return': {
                    'rc':0,
                } 
            }
            #@todo: error handling

        #########################################
        # Update Stream
        #########################################
        elif request.postpath[0]==b'update_stream':
            print("APIv1 update stream")
            streamURL = request.args[b'url'][0].decode()

            update = self.cR.streamingServer_i.updateStreamLocation(streamURL)
            status = {
                'return': {
                    'rc': 0 if update == True else 1,
                } 
            }
            #@todo: error handling

        #########################################
        # Stop Stream
        #########################################
        elif request.postpath[0]==b'stop_stream':
            print("APIv1 stop stream")
            self.cR.streamingServer_i.stop_pipeline()
            status = {
                'return': {
                    'rc':0,
                } 
            }
            
        #########################################
        # Start Receive
        #########################################
        elif request.postpath[0]==b'start_receive':
            print("APIv1 start receive")
            streamURL = request.args[b'url'][0].decode()

            self.cR.streamingServer_i.start_pipeline(streamURL, True)
            status = {
                'return': {
                    'rc':0,
                } 
            }
            #@todo: error handling

        #########################################
        # Update Receive
        #########################################
        elif request.postpath[0]==b'update_receive':
            print("APIv1 update receive")
            streamURL = request.args[b'url'][0].decode()
            
            update = self.cR.streamingServer_i.updateStreamLocation(streamURL)
            status = {
                'return': {
                    'rc': 0 if update == True else 1,
                } 
            }
            #@todo: error handling

        #########################################
        # Stop Receive
        #########################################
        elif request.postpath[0]==b'stop_receive':
            print("APIv1 stop receive")
            self.cR.streamingServer_i.stop_pipeline()
            status = {
                'return': {
                    'rc':0,
                } 
            }
        
        #return post
        return str.encode(json.dumps(status))
