# Streaming Server 2

Web App for Raspbery PI based Streaming Server

## Server API

* [System Service](doc/SystemService.md)
* [Conf Service](doc/ConfService.md)
* [Overlay Service](doc/OverlayService.md)
* [Streaming Service](doc/Streaming.md)
* [Janus Service](doc/Janus.md)

---

## Development Setup

### Raspberry PI: Requirements

Add additional system packages have to be installed:

```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3 python3-twisted python3-simplejson python3-gst-1.0
sudo apt-get install gstreamer1.0-plugins-good gstreamer1.0-plugins-base gstreamer1.0-plugins-bad
sudo apt-get install gstreamer1.0-omx gstreamer1.0-libav gstreamer1.0-alsa gstreamer1.0-tools
sudo apt-get install frei0r-plugins i2c-tools xorg
sudo apt-get install python3-prometheus-client python3-autobahn python3-pil
```

**Note:** This component will be deployed to `/opt/streaming-server` on the PIs.

### Install Python libaries

Additional Python libraries are required.

```sh
pip3 install pathvalidate
```

### Alternative: Windows WSL2

**⚠ Limitation:**
**No audio/video connections are possible.**

Use [Debian](https://www.microsoft.com/de-de/search/shop/apps?q=wsl&devicetype=pc&category=Developer+tools&Price=0) for the Windows Subsystem for Linux (WSL).

Follow instructions described at Raspberry PI to install system packages and Python libs.

## Configuration of local dev Environment

* Copy `streaming-server-local.conf.example` to `streaming-server-local.conf`
* Copy `streaming-server-global.conf.example` to `streaming-server-global.conf`

and update the config:

**//@TODO** What is useful/required for local dev?

## Run Server

```sh
python3 server.py
```

Now the Web App could be loaded: [http://localhost:8080]

---

## Contribution

//@TODO

### Coding Guide Lines

* Python
* JavaScript
* HTML, CSS
* vue.js

#### REST

* States should be send in String constants and addition meaningful english
  message text, e.g.:

  ```json
  {
    "state": "CONNECTION_FAILED",
    "message": "Connection to system failed."
  }
  ```

#### vue.js

The vue.js project has configured lint.

### Contributor

* Maik Ender, mail@maikender.de
* Carl-Daniel Hailfinger, carl-daniel@hailfinger.org
* Markus Leitold, markus.leitold@confisys.de
* Samuel Simmerling, samuel@simmerling.online
* ...
* //@TODO

## Licence
[AGPL-3.0](LICENCE)
