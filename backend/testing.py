# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0

import requests
import time

allTests=1

def test(name, urlEntry, payload, expectJson, expected):
    print("**************TESTING***************")
    print("*                                  *")
    print("*\t", name)
    print("*                                  *")
    print("**************TESTING***************")
    global allTests
    allTests+=1
    testFailed = False;
    
    r = requests.get("http://localhost:8080/"+urlEntry, params=payload)
    if (expectJson == True):
        response = r.json()
        print(response)
        print(expected)
        testFailed = False;
        for k in expected:
            if( response[k] != expected[k] ):
                testFailed = True
        
        
    else:
        response = r.text
        print(response)
        print(expected)
        if( response == expected):
            testFailed = False
        else:
            testFailed = True
            
    
    if( testFailed == True):
        print("test FAILED");
        print(response)
        return 0
    else:
        print("test PASSED");
        return 1


def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def getHostname():
    
    r = requests.get("http://localhost:8080/conf", params={'getROProperties': ' '}).json()
    return remove_prefix(r['hostname'], 'pi-')
    


if __name__ == "__main__":
    
    print("testing started")
    successFullTests = 0
    
    #ensure stream is stopped
    successFullTests += test("Stream Start", "conf", {'streaming': 'stop'}, True, {'return': 'ok'})
    time.sleep(5)
    
    # WITHOUT STREAMING
    successFullTests += test("Stream Status", "conf", {'status': ' '}, True, {'status': 'STOPPED'})
    
    successFullTests += test("Set Volume", "conf", {'setProperty': 'volume', 'value': '6'}, True, {'return': 'true'})
    successFullTests += test("Get Volume", "conf", {'getProperty': 'volume'}, True, {'return': '6'})
    successFullTests += test("Set Volume", "conf", {'setProperty': 'volume', 'value': '5'}, True, {'return': 'true'})
    successFullTests += test("Get Volume", "conf", {'getProperty': 'volume'}, True, {'return': '5'})
    
    
    successFullTests += test("Set wbRed", "conf", {'setProperty': 'wbRed', 'value': '1'}, True, {'return': 'true'})
    successFullTests += test("Get wbRed", "conf", {'getProperty': 'wbRed'}, True, {'return': '1'})
    successFullTests += test("Set wbRed", "conf", {'setProperty': 'wbRed', 'value': '0'}, True, {'return': 'true'})
    successFullTests += test("Get wbRed", "conf", {'getProperty': 'wbRed'}, True, {'return': '0'})
    
    successFullTests += test("Set wbGreen", "conf", {'setProperty': 'wbGreen', 'value': '1'}, True, {'return': 'true'})
    successFullTests += test("Get wbGreen", "conf", {'getProperty': 'wbGreen'}, True, {'return': '1'})
    successFullTests += test("Set wbGreen", "conf", {'setProperty': 'wbGreen', 'value': '0'}, True, {'return': 'true'})
    successFullTests += test("Get wbGreen", "conf", {'getProperty': 'wbGreen'}, True, {'return': '0'})
    
    
    successFullTests += test("Set wbBlue", "conf", {'setProperty': 'wbBlue', 'value': '1'}, True, {'return': 'true'})
    successFullTests += test("Get wbBlue", "conf", {'getProperty': 'wbBlue'}, True, {'return': '1'})
    successFullTests += test("Set wbBlue", "conf", {'setProperty': 'wbBlue', 'value': '0'}, True, {'return': 'true'})
    successFullTests += test("Get wbBlue", "conf", {'getProperty': 'wbBlue'}, True, {'return': '0'})

    successFullTests += test("Set videoBitrate", "conf", {'setProperty': 'videoBitrate', 'value': '1000000'}, True, {'return': 'true'})
    successFullTests += test("Get videoBitrate", "conf", {'getProperty': 'videoBitrate'}, True, {'return': '1000000'})
    successFullTests += test("Set videoBitrate", "conf", {'setProperty': 'videoBitrate', 'value': '1000001'}, True, {'return': 'true'})
    successFullTests += test("Get videoBitrate", "conf", {'getProperty': 'videoBitrate'}, True, {'return': '1000001'})
    
    
    # WITH STREAMING
    successFullTests += test("Stream Status", "conf", {'status': ' '}, True, {'status': 'STOPPED'})
    successFullTests += test("Stream Start", "conf", {'streaming': 'start'}, True, {'return': 'ok'})
    time.sleep(5)
    successFullTests += test("Stream Status", "conf", {'status': ' '}, True, {'status': 'PLAYING'})
    
    
    successFullTests += test("Set Volume", "conf", {'setProperty': 'volume', 'value': '6'}, True, {'return': None})
    successFullTests += test("Get Volume", "conf", {'getProperty': 'volume'}, True, {'return': 6.0})
    successFullTests += test("Set Volume", "conf", {'setProperty': 'volume', 'value': '5'}, True, {'return': None})
    successFullTests += test("Get Volume", "conf", {'getProperty': 'volume'}, True, {'return': 5.0})
    
    
    successFullTests += test("Set wbRed", "conf", {'setProperty': 'wbRed', 'value': '1'}, True, {'return': None})
    successFullTests += test("Get wbRed", "conf", {'getProperty': 'wbRed'}, True, {'return': 1.0})
    successFullTests += test("Set wbRed", "conf", {'setProperty': 'wbRed', 'value': '0'}, True, {'return': None})
    successFullTests += test("Get wbRed", "conf", {'getProperty': 'wbRed'}, True, {'return': 0.0})
    
    successFullTests += test("Set wbGreen", "conf", {'setProperty': 'wbGreen', 'value': '1'}, True, {'return': None})
    successFullTests += test("Get wbGreen", "conf", {'getProperty': 'wbGreen'}, True, {'return': 1.0})
    successFullTests += test("Set wbGreen", "conf", {'setProperty': 'wbGreen', 'value': '0'}, True, {'return': None})
    successFullTests += test("Get wbGreen", "conf", {'getProperty': 'wbGreen'}, True, {'return': 0.0})
    
    
    successFullTests += test("Set wbBlue", "conf", {'setProperty': 'wbBlue', 'value': '1'}, True, {'return': None})
    successFullTests += test("Get wbBlue", "conf", {'getProperty': 'wbBlue'}, True, {'return': 1.0})
    successFullTests += test("Set wbBlue", "conf", {'setProperty': 'wbBlue', 'value': '0'}, True, {'return': None})
    successFullTests += test("Get wbBlue", "conf", {'getProperty': 'wbBlue'}, True, {'return': 0.0})

    successFullTests += test("Set videoBitrate", "conf", {'setProperty': 'videoBitrate', 'value': '1000000'}, True, {'return': None})
    successFullTests += test("Get videoBitrate", "conf", {'getProperty': 'videoBitrate'}, True, {'return': 1000000})
    successFullTests += test("Set videoBitrate", "conf", {'setProperty': 'videoBitrate', 'value': '1000001'}, True, {'return': None})
    successFullTests += test("Get videoBitrate", "conf", {'getProperty': 'videoBitrate'}, True, {'return': 1000001})
    

    successFullTests += test("Set textoverlay", "conf", {'setProperty': 'textoverlay', 'value': 'BLA'}, True, {'return': None})
    successFullTests += test("Get textoverlay", "conf", {'getProperty': 'textoverlay'}, True, {'return': 'BLA'})
    successFullTests += test("Set textoverlay", "conf", {'setProperty': 'textoverlay', 'value': 'BLUB'}, True, {'return': None})
    successFullTests += test("Get textoverlay", "conf", {'getProperty': 'textoverlay'}, True, {'return': 'BLUB'})
    
    print("Now test mannually if the stream is available at: http://cdn.nak-west.de/live/"+getHostname())
    input("Press Enter to continue...")

    successFullTests += test("Stream Stop", "conf", {'streaming': 'stop'}, True, {'return': 'ok'})
    

    print("\n\n\n\n\n\n\n")
    print("finished")
    allTests-=1
    print(" ", successFullTests, " of ", allTests, "Tests successfull")
