# Copyright 2020-2021, Maik Ender and Carl-Daniel Hailfinger and the Hai-End Streaming project
# SPDX-License-Identifier: AGPL-3.0

class ClassRegistry():
    streamingServer_i = None
    overlayGen_i = None
    streamState_i = None

    
    def setStreamingServer_i(self, s):
        self.streamingServer_i = s
    
    def setOverlayGen_i(self, g):
        self.overlayGen_i = g

    def setStreamState_i(self, s):
        self.streamState_i = s